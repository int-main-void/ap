class CreateSystemRoles < ActiveRecord::Migration
  def change
    create_table :system_roles do |t|
      t.string :name

      t.integer :lock_version, :default => 0
      t.timestamps null: false
    end
  end
end
