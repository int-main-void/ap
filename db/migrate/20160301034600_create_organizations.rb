class CreateOrganizations < ActiveRecord::Migration
  def change
    create_table :organizations do |t|

      t.string :name
      t.string :business_id

      t.integer :lock_version, :default => 0
      t.timestamps null: false
    end
  end
end
