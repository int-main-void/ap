json.array!(@sites) do |site|
  json.extract! site, :id, :street1, :street2, :city, :state, :postal_code, :country, :latitude, :longitude
  json.url site_url(site, format: :json)
end
