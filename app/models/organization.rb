class Organization < ActiveRecord::Base
  has_many :roles
  has_many :projects
  has_many :organization_sites
end
