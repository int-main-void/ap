class AuthController < ApplicationController

  skip_before_action :authorize, :only => [:signin, :signup]

  def signin
    # TODO - check if logged in - can't sign in until you're logged out
    session[:user_id] = nil
    if request.post?
      user = User.find_by_email(params[:email])
      if user && user.authenticate(params[:password])
        session[:user_id] = user.id
        redirect_to "/" # TODO - redirect to where they came from
      else
        flash.now[:danger] = "Invalid email or password"
      end
    end
  end

  def signup
    # TODO - check if logged in. can't sign up until you log out

    if request.post?
      begin
        user = User.new(name: params[:name], email: params[:email], 
                        password: params[:password], 
                        password_confirmation: params[:password_confirmation])
        user.save!
      rescue Exception => e
        flash.now[:danger] = e # HACKME - clean this up
      end
      if user && user.id
        session[:user_id] = user.id
        redirect_to "/" # TODO - redirect to where they came from
      else
        flash.now[:danger] = "unable to create user"
      end
    end
    # TODO - create new account
  end

  def signout
    if request.post?
      if params[:logout_confirmed]
        session[:user_id] = nil
        redirect_to "/" 
      end
    end
  end


end
