class CreateRoles < ActiveRecord::Migration
  def change
    create_table :roles do |t|

      t.string :name
      t.integer :organization_id

      t.integer :lock_version, :default => 0
      t.timestamps null: false
    end
  end
end
