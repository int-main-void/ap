class CreateUserRoles < ActiveRecord::Migration
  def change
    create_table :user_roles do |t|

      t.integer :user_id
      t.integer :role_id

      t.integer :lock_version, :default => 0
      t.timestamps null: false
    end
  end
end
