json.array!(@user_sites) do |user_site|
  json.extract! user_site, :id, :user_id, :site_id, :start_date, :end_date
  json.url user_site_url(user_site, format: :json)
end
