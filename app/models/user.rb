class User < ActiveRecord::Base
  has_secure_password

  has_many :user_roles
  has_and_belongs_to_many :system_roles
  has_and_belongs_to_many :roles
  has_many :user_sites

  validates :password, length: { minimum: 6, maximum: 72 }

  validates_uniqueness_of :name
  validates :name, length: { minimum: 6, maximum: 24 }

  validates_uniqueness_of :email
  validates :email, :format => /.+@.+\..+/, length: { maximum: 48 }

  private

  def user_params
    params.require(:user).permit(:name, :email, :password, :password_confirmation)
  end
end
