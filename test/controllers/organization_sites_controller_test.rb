require 'test_helper'

class OrganizationSitesControllerTest < ActionController::TestCase
  setup do
    @organization_site = organization_sites(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:organization_sites)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create organization_site" do
    assert_difference('OrganizationSite.count') do
      post :create, organization_site: { organization_id: @organization_site.organization_id, site_id: @organization_site.site_id }
    end

    assert_redirected_to organization_site_path(assigns(:organization_site))
  end

  test "should show organization_site" do
    get :show, id: @organization_site
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @organization_site
    assert_response :success
  end

  test "should update organization_site" do
    patch :update, id: @organization_site, organization_site: { organization_id: @organization_site.organization_id, site_id: @organization_site.site_id }
    assert_redirected_to organization_site_path(assigns(:organization_site))
  end

  test "should destroy organization_site" do
    assert_difference('OrganizationSite.count', -1) do
      delete :destroy, id: @organization_site
    end

    assert_redirected_to organization_sites_path
  end
end
