json.array!(@organization_sites) do |organization_site|
  json.extract! organization_site, :id, :organization_id, :site_id
  json.url organization_site_url(organization_site, format: :json)
end
