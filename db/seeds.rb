# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)


organization = Organization.create({name: 'MTNW' })
project = Project.create({name: 'proj1', organziation: organization})
Role.create({name: 'admin', organization: organization})
Role.create({name: 'user', organization: organization})

SystemRole.create({name:'admin'})

