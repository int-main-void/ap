class AdminController < ApplicationController

  # TODO - control admin access based on system roles
  before_filter :authorize_admin


  protected

  def authorize_admin
    user = User.find_by_id(session[:user_id])
    user.system_roles.each do |sr|
      if  "admin" == sr.name
        return
      end
    end

    redirect_to "/"
  end

end
