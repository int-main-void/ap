class SystemRole < ActiveRecord::Base
  has_one :server_role
  has_and_belongs_to_many :user
end
