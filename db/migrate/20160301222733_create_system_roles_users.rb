class SystemRolesUsers < ActiveRecord::Migration
  def change
    create_table :system_roles_users, id: false do |t|
      t.belongs_to :user, index: true
      t.belongs_to :system_role, index: true
    end
  end
end
