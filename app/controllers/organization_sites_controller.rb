class OrganizationSitesController < ApplicationController
  before_action :set_organization_site, only: [:show, :edit, :update, :destroy]

  # GET /organization_sites
  # GET /organization_sites.json
  def index
    @organization_sites = OrganizationSite.all
  end

  # GET /organization_sites/1
  # GET /organization_sites/1.json
  def show
  end

  # GET /organization_sites/new
  def new
    @organization_site = OrganizationSite.new
  end

  # GET /organization_sites/1/edit
  def edit
  end

  # POST /organization_sites
  # POST /organization_sites.json
  def create
    @organization_site = OrganizationSite.new(organization_site_params)

    respond_to do |format|
      if @organization_site.save
        format.html { redirect_to @organization_site, notice: 'Organization site was successfully created.' }
        format.json { render :show, status: :created, location: @organization_site }
      else
        format.html { render :new }
        format.json { render json: @organization_site.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /organization_sites/1
  # PATCH/PUT /organization_sites/1.json
  def update
    respond_to do |format|
      if @organization_site.update(organization_site_params)
        format.html { redirect_to @organization_site, notice: 'Organization site was successfully updated.' }
        format.json { render :show, status: :ok, location: @organization_site }
      else
        format.html { render :edit }
        format.json { render json: @organization_site.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /organization_sites/1
  # DELETE /organization_sites/1.json
  def destroy
    @organization_site.destroy
    respond_to do |format|
      format.html { redirect_to organization_sites_url, notice: 'Organization site was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_organization_site
      @organization_site = OrganizationSite.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def organization_site_params
      params.require(:organization_site).permit(:organization_id, :site_id)
    end
end
