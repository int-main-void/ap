class OrganizationSite < ActiveRecord::Base
  belongs_to :organization
  has_one :site
end
