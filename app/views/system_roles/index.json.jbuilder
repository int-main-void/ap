json.array!(@system_roles) do |system_role|
  json.extract! system_role, :id, :name
  json.url system_role_url(system_role, format: :json)
end
