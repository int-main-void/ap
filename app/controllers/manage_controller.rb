class ManageController < ApplicationController
  before_action :authorize_org

  private

  def authorize_org
    user = User.find_by_id(session[:user_id])
    session_org = session[:org_id]
    user.roles.each do |role|
      if role.organization_id == session_org &&  role.name == 'admin'
        return
      end
    end
    redirect_to "/"
  end

end
