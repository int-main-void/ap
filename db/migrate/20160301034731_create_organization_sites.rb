class CreateOrganizationSites < ActiveRecord::Migration
  def change
    create_table :organization_sites do |t|

      t.integer :organization_id
      t.integer :site_id

      t.integer :lock_version, :default => 0
      t.timestamps null: false
    end
  end
end
