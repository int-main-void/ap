Rails.application.routes.draw do

  resources :organization_sites
  resources :user_sites
  resources :sites
  resources :user_server_roles
  resources :user_roles
  resources :roles
  resources :organizations
  resources :users
  resources :system_roles
  resources :projects

  root 'welcome#index'

  get 'login' => 'auth#signin'
  post 'login' => 'auth#signin'

  get 'signup' => 'auth#signup'
  post 'signup' => 'auth#signup'

  get 'logout' => 'auth#signout'
  post 'logout' => 'auth#signout'

  get 'admin' => 'admin#index'

  get 'gallery' => 'gallery#index'

  get 'manage' => 'manage#index'

  get 'account' => 'account#index'

  get 'capture' => 'capture#index'

end
