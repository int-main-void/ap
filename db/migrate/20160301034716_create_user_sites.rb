class CreateUserSites < ActiveRecord::Migration
  def change
    create_table :user_sites do |t|

      t.integer :user_id
      t.integer :site_id
      t.date :start_date
      t.date :end_date

      t.integer :lock_version, :default => 0
      t.timestamps null: false
    end
  end
end
